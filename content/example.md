# Exempel

Det här exemplet visar på hur man kommunicerar med Ånges Anslagstavle via det öppna API:et.

## Hämta anslagsöversikten 

För att hämta alla publicerade anslag.

### Fråga:

```json
curl -H "Accept: application/json" https://moten.ange.se/api/dbb/v1.0/announcements/published/
```
#### Svar:

```json
{
  "Items": [
    {
      "Id": "80ed74be-162e-47ea-87af-5f61de488b6a",
      "Type": "Kommunfullmäktige protokoll",
      "StartDate": "2024-03-01",
      "EndDate": "2024-03-22",
      "Title": "Protokoll från möte i Kommunfullmäktige - 2024-02-26",
      "CommitteeName": "Kommunfullmäktige",
      "CommitteeLink": "committees/kommunfullmaktige"
    },
    {
      "Id": "37dee4d3-bf5d-4942-a7fc-236ab079a933",
      "Type": "Valnämndens protokoll",
      "StartDate": "2024-02-29",
      "EndDate": "2024-03-21",
      "Title": "Protokoll från möte i Valnämnden - 2024-02-28",
      "CommitteeName": "Valnämnden",
      "CommitteeLink": "committees/valnamnden"
    },
    {
      "Id": "ff7e7e09-d485-4545-80a1-c356013e63f1",
      "Type": "Kommunfullmäktige sammanträde",
      "StartDate": "2024-02-22",
      "EndDate": "2024-03-04",
      "Title": "Kommunfullmäktige sammanträder 2024-02-26",
      "CommitteeName": "Kommunfullmäktige",
      "CommitteeLink": "committees/kommunfullmaktige"
    },
    ...
  ],
  "TotalItemCount": 6
}
```

## Hämta ett specifikt anslag
För att hämta ett anslag behöver man veta id:t på anslaget som finns i översikten, t.ex. 

```
...
"Id": "v",
...
```

#### Fråga
Så här t.ex. hämtas anslaget med id "ff7e7e09-d485-4545-80a1-c356013e63f1"
```
curl -H "Accept: application/json" https://moten.ange.se/api/dbb/v1.0/announcements/ff7e7e09-d485-4545-80a1-c356013e63f1
```

### Svar
Så här ser ett svar ut.

```json
{
    "Id": "e713a832-f7fe-4194-82d8-0fa54ed0274b",
    "TypeId": "98bf16d7-746b-4aef-bcc9-3c1c5b88bd3b",
    "Title": "Protokoll från möte i Kommunstyrelsen - 2024-02-13",
    "Description": "",
    "PublishedFrom": "2024-02-20",
    "PublishedTo": "2024-03-12",
    "ApprovedToBePublished": true,
    "MeetingProtocolUri": "/sv/committees/kommunstyrelsen/mote-2024-02-13#protocolTabContent",
    "MeetingId": 31031,
    "MeetingDate": "2024-02-13",
    "ProtocolAttachmentId": 33228,
    "ProtocolAttachmentTitle": "Protokoll KS 2024-02-13.pdf",
    "ProtocolAttachmentUri": "/sv/committees/kommunstyrelsen/mote-2024-02-13/protocol/protokoll-ks-2024-02-13pdf",
    "ProtocolParagraphs": "",
    "ResponsiblePerson": "",
    "ProtocolStorage": "",
    "CanBeEdited": false,
    "CanBeDeleted": false,
    "CommitteeName": "Kommunstyrelsen",
    "CommitteeLink": "committees/kommunstyrelsen"
}
```

### Hämta ett protokoll
Här är ett exemple på hur man hämtar ett protokoll

#### Fråga
Exempel på hur man kan använda curl för att ladda ner och spara dokumenetet.
```json
curl -OJL https://moten.ange.se/sv/committees/utbildningsutskott/mote-2023-10-06/protocol/protokoll-utbildningsutskottet-2023-10-06pdf

```

### Svar
Dokumentet sparas lokalt i samma katalog som man är i när man kör kommandot.
```
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  828k  100  828k    0     0  5605k      0 --:--:-- --:--:-- --:--:-- 5711k

```