# Exempel i JSON

JSON exempel.

```json
{
    "Id":  "X-Y89",
    "Title":  "Kungörelse om flyttat fordon",
    "Description":  "Fordon har flyttats vid exempelvägen",
    "PublishedFrom": "2023:05:30:T08:30",
    "PublishedTo":  "2023:05:30:T09:30",
    "ApprovedToBePublished":  "true",
    "CommitteeName":  "Myndighetsnämnden",
    "CommitteeId":  "901",
    "CommitteeLink":  "https://example.com/samhallsbyggnadsforvatlningen",  
}
```
