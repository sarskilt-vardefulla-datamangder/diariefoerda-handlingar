# RDF/XML - exempel översiktsnivå

``` xml
<?xml version="1.0"?>
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	xmlns:dcterms="http://purl.org/dc/terms/"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
  xmlns:schema="http://schema.org">
  <rdf:Description rdf:about="http://example.com">
    <dcterms:identifier>893454-EX</dcterms:identifier>
    <dcterms:title>Announcement</dcterms:title>
    <dcterms:description xml:lang="en">Announcement about moving wrongfully parked vehice at example street.</dcterms:description>
    <dcterms:issued rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">2023-08-15T10:00:00.000Z</dcterms:issued>
    <dcterms:valid rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">2023-08-21T22:00:00.000Z</dcterms:valid>
        <dcterms:publisher>Myndighetsnämnden</dcterms:publisher>
        <dcterms:identifier rdf:datatype="http://www.w3.org/2001/XMLSchema#string">901-urn234-09</dcterms:identifier>
        <schema:url rdf:resource="https://example.com/samhallsbyggnadsforvatlningen"></schema:url>
      </rdf:Description>
  </rdf:RDF>
