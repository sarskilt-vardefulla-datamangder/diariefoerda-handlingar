**Bakgrund**

Syftet med denna specifikation är att beskriva information om offentliga anslag på ett enhetligt och standardiserat vis. 

Specifikationen syftar till att ge kommuner i Sverige möjlighet att enkelt kunna sätta samman och publicera datamängd(er) som beskriver offentliga anslag. Den syftar även till att göra det enklare för internationella användare som är tagare av datamängden.

Följande har deltagit:

**[Dataverkstad](https://www.vgregion.se/ov/dataverkstad/)** - Modellering och utförande.<br>
**[Formpipe](https://www.formpipe.com/sv/)** - Domänkunskap och rådgivning.<br>

