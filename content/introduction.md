# Introduktion 

Denna specifikation anger vilka fält som är obligatoriska och rekommenderade. T.ex. gäller att man måste ange TypeId, Title och Id. 10 attribut kan anges varav 4 är obligatoriska. 

I appendix A finns ett exempel på hur en entitet uttrycks i JSON. I appendix B uttrycks samma exempel i RDF. 

Denna specifikation definierar en enkel informationsmodell för offentliga anslag. Specifikationen innefattar också en beskrivning av hur informationen uttrycks i formaten RDF och JSON. 