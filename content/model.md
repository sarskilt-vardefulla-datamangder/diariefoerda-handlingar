# Datamodell - översiktsnivå

Datamodellen är i JSON, det innebär att varje objekt motsvarar ett anslag med flera egenskaper. 10 attribut är definierade, där de första 3 är obligatoriska. 

<div class="note" title="1">
Vi väljer att använda beskrivande men korta namn som uttrycks med gemener, utan mellanslag och på engelska.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige.


</div>

<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
| -- | :---------------:| :-------------------------------: | ---------------------- |
|[**Id**](#id)|1|text|**Obligatoriskt** - Anger identifierare för anslaget.|
|[**Title**](#type)|1|text|**Obligatoriskt** - Anger anslagets titel.|
|[**Description**](#Description)|1|text|**Obligatoriskt** - Anger en kortare beskrivning av anslaget. |  
|[Type](#Type)|0..1|text|Anger typen av anslag. |
|[PublishedFrom](#PublishedFrom)|0..1|dateTime| Anger när anslaget publicerades.|
|[PublishedTo](#PublishedTo)|0..1|dateTime|Anger när anslaget avpubliceras.|
|[ApprovedToBePublished](#ApprovedToBePublished)|0..1|boolean|Anger godkännandet av anslaget. |
|[CommitteeName](#CommitteeName)|0..1|text|Anger namnet på nämnden.|
|[CommitteeId](#CommitteeId)|0..1|heltal|Anger id för nämnden.|
|[CommitteeLink](#CommitteeLink)|0..1|URL|Länk till ingångssidan hos den lokala myndigheten för nämnden.|

</div>

# Datamodell -detaljnivå

Datamodellen är i JSON, det innebär att varje objekt motsvarar ett anslag med flera egenskaper. 11 attribut är definierade, där de första 3 är obligatoriska. 

<div class="note" title="1">
Vi väljer att använda beskrivande men korta namn som uttrycks med gemener, utan mellanslag och på engelska.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige.


</div>

<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
| -- | :---------------:| :-------------------------------: | ---------------------- |
|[**Id**](#id)|1|text|**Obligatoriskt** - Anger identifierare för anslaget.|
|[**Title**](#type)|1|text|**Obligatoriskt** - Anger anslagets titel.|
|[**Description**](#Description)|1|text|**Obligatoriskt** - Anger en kortare beskrivning av anslaget. |  
|[ProtocolStorage](#ProtocolStorage)|0..1|text|Anger platsen där handlingen förvaras. |
|[MeetingDate](#MeetingDate)|0..1|dateTime|Anger datum då mötet skedde. |
|[PublishedFrom](#PublishedFrom)|0..1|dateTime| Anger när anslaget publicerades.|
|[PublishedTo](#PublishedTo)|0..1|dateTime|Anger när anslaget avpubliceras.|
|[ApprovedToBePublished](#ApprovedToBePublished)|0..1|boolean|Anger godkännandet av anslaget. |
|[DocumentKind](#DocumentKind)|0..1|text|Anger typen av handling. |
|[DocumentTitle](#DocumentTitle)|0..1|text|Anger titeln på handlingen. |
|[DocumentLink](#DocumentLink)|0..1|URL|Anger länk till handlingen. |


</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, dvs inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
### **decimal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**

En länk till en webbsida där entiteten presenteras hos den lokala myndigheten eller organisation.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "https://www.example.com" ok. Relativa webbadresser accepteras inte heller. (Ett fullständigt reguljärt uttryck utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

### **boolean**

Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad Boolesk datatyp och kan antingen ha ett av två värden: TRUE eller FALSE, men aldrig båda.

### **dateTime** 

Reguljärt uttryck: **`\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(?:\.\d+)?Z?`**

dateTime värden kan ses som objekt med heltalsvärdena år, månad, dag, timme och minut, en decimalvärderad sekundegenskap och en tidszonegenskap.

[XML schema](https://www.w3.org/TR/xmlschema-2/#dateTime) 
</br>

[IS0 8601](https://en.wikipedia.org/wiki/ISO_8601)

Värden som kan anges för att beskriva ett datum:

YYYY-MM-DD
</br>
YYYY-MM
</br>
YYYYMMDD 
</br>
</div>

Observera att ni inte kan ange: **YYYYMM**    

För att beskriva tider uttrycker ni det enligt nedan:

**T** representerar tid, **hh** avser en nollställd timme mellan 00 och 24, **mm** avser en nollställd minut mellan 00 och 59 och **ss** avser en nollställd sekund mellan 00 och 60.

Thh:mm:ss.sss eller	Thhmmss.sss 
</br>
Thh:mm:ss eller Thhmmss
</br>
Thh:mm eller Thhmm
</br>

</div>

## Förtydligande av attribut

### **Id**

Reguljärt uttryck: **`/^[a-zA-Z_:1-9]*$/`**

Ange identifierare för anslaget. 

### Title

Anslagets titel. 

### Description

En kortare beskrivning av anslaget.

### PublishedFrom

När anslaget publicerades av den lokala myndigheten. 


### PublishedTo

När anslaget avpubliceras av den lokala myndigheten.  

### ApprovedToBePublished

Godkännandet att publicera anslaget. 

### CommitteeName

Nämndens namn. 

### CommitteeId

Id för nämnden.

### CommitteeLink

Länk till ingångssidan hos den lokala myndigheten för nämnden. Ange inledande schemat https:// eller http://

### MeetingDate

Datum då mötet skedde, anges enligt [dateTime](#datetime)

### ProtocolStorage

Platsen där handlingen finns, kan exempelvis som **Sundsvalls Kommun**

### DocumentKind

Typen av handling, kan anges exempelvis som  **InternalProtocol**

### DocumentTitle 

Titeln på handlingen för anslaget, kan anges exempelvis som **Överförmyndarnämnden Mitt 2023-11-24**

### DocumentLink 

Länk till handlingen. 


 




